import pdb
import numpy as np
import netCDF4 as nc4

from contextlib import closing



class Region(object):

    def __init__(self, minx, maxx, miny, maxy, xvname='x', yvname='y', sample_data=None):
        self.maxx = max(minx, maxx)
        self.minx = min(minx, maxx)
        self.maxy = max(miny, maxy)
        self.miny = min(miny, maxy)

        self.xvname = xvname
        self.yvname = yvname

        self.x      = None
        self.y      = None
        self.xmask  = None
        self.ymask  = None

        if sample_data is not None:
            self.initialize(sample_data)


    def initialize(self,sample_data_path):
        with closing(nc4.Dataset(sample_data_path)) as ifile:
            self.x = ifile.variables[self.xvname][:]
            self.y = ifile.variables[self.yvname][:]

        self.xmask = np.logical_and(self.x >= self.minx, self.x <= self.maxx)
        self.ymask = np.logical_and(self.y >= self.miny, self.y <= self.maxy)


    def slice(self, path, vname):
        data = None
        with closing(nc4.Dataset(sample_data_path)) as ifile:
            data = ifile.variables[vname][:]

        data = data[:, self.ymask]
        data = data[:,:, self.xmask]

        return data


def test():
    PATH = '/g/data/rr5/satellite/obs/himawari8/FLDK/2017/01/01/0000/20170101000000-P1S-ABOM_OBS_B02-PRJ_GEOS141_2000-HIMAWARI8-AHI.nc'

    region = Region(minx=-100, maxx=100, miny=-100, maxy=100, xvname='x', yvname='y', sample_data=PATH)

   for fn in files:
       region.slice(fn, vname)

    pdb.set_trace()


if __name__ == '__main__':
    test()
