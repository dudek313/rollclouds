


import pdb
import netCDF4 as nc4
import numpy as np
import matplotlib.pyplot as plt


PATH = '/g/data/rr5/satellite/obs/himawari8/FLDK/2016/01/18/2230/20160118223000-P1S-ABOM_OBS_B02-PRJ_GEOS141_2000-HIMAWARI8-AHI.nc'
VNAME = 'channel_0002_scaled_radiance'

ifile = nc4.Dataset(PATH)
pdb.set_trace()

data = ifile.variables[VNAME][:]
data = data.squeeze()

print data.max(), data.min()

plt.pcolormesh(data>0.5)
plt.colorbar()
plt.show()



