import numpy as np
import netCDF4 as nc4
import matplotlib.pyplot as plt


PATH='/g/data/rr5/satellite/obs/himawari8/FLDK/2015/11/13/0000/20151113000000-P1S-ABOM_OBS_B06-PRJ_GEOS141_2000-HIMAWARI8-AHI.nc'

ifile=nc4.Dataset(PATH)

dir(ifile)
ifile.variables.keys()
bt=ifile.variables['channel_0006_scaled_radiance']
type(bt)
dir(bt)
data=bt[:].squeeze()
data.shape
type(data)
plt.pcolormesh(data)
plt.colorbar()
plt.show()
bt.shape
